# This project is now Archived
This project was a great learning experience for me. It made me understand the installation process of [Arch Linux](https://archlinux.org) so much better and in more detail than I did before. However, I am dropping the use of it in favor of [Anarchy Installer](https://anarchyinstaller.gitlab.io/). The one downside is that I am going back to having to go through menus and select my settings manually, instead of automating the whole thing. Both methods are about the same in terms of installation time, so rather than maintain a script I'll rely on an already maintained script.

# Archify
## An Automated Arch Installer
After many Arch re-installs over the years I grew tired of the process.

I got tired of typing the same thing over and over and over again. That's
the sort of thing we have computers for... So, here's yet another automated way to install Arch.

At first I used EndeavourOS, because it's close to stock. It was an amazing experience. Also an amazing community (which I am glad to still be a part of).
But it didn't sit quite right with me, though, so I went and gave Anarchy installer a whirl. That was awesome. "Real Arch" through a menu. A menu... A "pick everything when it's time to do so"-menu... This ment constantly needing to navigate through menus... I just want to ***press a button*** and be set.


### How to use
First, **read through the code**! There are several things that need to be altered if you want to use this for yourself.

Second get a copy of a fresh Arch Linux ISO at https://archlinux.org/download/

Next burn it on a disk or usb drive, or use in Virtualbox. (If using this, best test in a VM first!)

Start the machine and wait for the prompt. Next let's get the program. 
```
curl -L tiny.cc/archify > archify
```

Now launch the program with the "start" flag. 
```
sh archify start
```
You will be asked to answer a couple quick questions. Now it's time to grab a cup of coffee.

When you're back it should be ready to chroot into your brand new Arch install and finish the setup. 
```
sh archify res
```

Answer a couple more questions, than sit back and enjoy your coffee!


### How else to use
Grab yourself a copy and edit to your heart's content! 

Have fun! =)


### Warrenty

This program was written for me, with my needs in mind. It works FOR ME. I take no responsibility for
any mishaps on your system. Use at your own risk! This program WILL delete everything on your hard drive!
